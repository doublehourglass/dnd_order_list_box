using Gtk;
using Gdk;
namespace DHGWidgets {

public class DndOrderListBoxBindings : Object {

public struct KeyBindingParam<G> {
	Type type;
	G param;
	public KeyBindingParam(G p, Type t) {
		this.param = p;
		this.type = t;
		}
	}

public struct KeyBinding {
	uint key;
	Gdk.ModifierType modifier;
	string signal_name;
	KeyBindingParam[] params;
	public KeyBinding(uint k, Gdk.ModifierType m, string s, KeyBindingParam[] p) {
		this.key = k;
		this.modifier = m;
		this.signal_name = s;
		this.params = p;
		}
	}

public struct MoveKeyBinding {
	KeyBindingParam[] params;
	MoveKeyBinding(MovementStep step, int count) {
		params = {
			KeyBindingParam<MovementStep>(step, typeof( MovementStep )),
			KeyBindingParam<int>(count, typeof( int ))
			};
		}
	}

public const Gdk.ModifierType MSHFT = Gdk.ModifierType.SHIFT_MASK;
public const Gdk.ModifierType MCTRL = Gdk.ModifierType.CONTROL_MASK;
public const Gdk.ModifierType MNONE = ( Gdk.ModifierType ) 0;

public static KeyBinding[] KeyBindings = {
	KeyBinding(Gdk.Key.Up, MCTRL, "kbd_move_indicator", {KeyBindingParam<bool>(true, typeof( bool ))}),
	KeyBinding(Gdk.Key.Down, MCTRL, "kbd_move_indicator", {KeyBindingParam<bool>(false, typeof( bool ))}),
	KeyBinding(Gdk.Key.Return, MNONE, "kbd_confirm", {}),
	KeyBinding(Gdk.Key.Escape, MNONE, "kbd_break", {}),
	KeyBinding(Gdk.Key.Home, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, -1).params),
	KeyBinding(Gdk.Key.KP_Home, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, -1).params),
	KeyBinding(Gdk.Key.End, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, 1).params),
	KeyBinding(Gdk.Key.KP_End, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, 1).params),
	KeyBinding(Gdk.Key.Page_Up, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.PAGES, -1).params),
	KeyBinding(Gdk.Key.KP_Page_Up, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.PAGES, -1).params),
	KeyBinding(Gdk.Key.Page_Down, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.PAGES, 1).params),
	KeyBinding(Gdk.Key.KP_Page_Down, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.PAGES, 1).params),
	KeyBinding(Gdk.Key.Up, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, -1).params),
	KeyBinding(Gdk.Key.KP_Up, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, -1).params),
	KeyBinding(Gdk.Key.Down, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, 1).params),
	KeyBinding(Gdk.Key.KP_Down, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, 1).params),
	};

public static KeyBinding[] AlternativeKeyBindings = {
	KeyBinding(Gdk.Key.i, MNONE, "kbd_vim", {}),
	KeyBinding(Gdk.Key.k, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, -1).params),
	KeyBinding(Gdk.Key.j, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, 1).params),
	KeyBinding(Gdk.Key.h, MSHFT, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, -1).params),
	KeyBinding(Gdk.Key.l, MSHFT, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, 1).params),
	KeyBinding(Gdk.Key.x, MNONE, "kbd_break", {}),
	};

[CCode (has_construct_function = false)]
public static DndOrderListBoxBindings(BindingSet binding_set) {
	install_bindings(binding_set, KeyBindings);
	install_bindings(binding_set, AlternativeKeyBindings);
	}

public static void install_bindings(BindingSet bind_set, KeyBinding[] binding_list) {
	foreach ( KeyBinding k in binding_list ) {
		switch ( k.params.length ) {
		case 0:
			Gtk.BindingEntry.add_signal(bind_set, k.key, k.modifier, k.signal_name, 0);
			break;
		case 1:
			Gtk.BindingEntry.add_signal(bind_set, k.key, k.modifier, k.signal_name, 1, k.params[0].type, k.params[0].param);
			break;
		case 2:
			Gtk.BindingEntry.add_signal(bind_set, k.key, k.modifier, k.signal_name, 2, k.params[0].type, k.params[0].param, k.params[1].type, k.params[1].param);
			break;
			}
		}
	}

}
}
