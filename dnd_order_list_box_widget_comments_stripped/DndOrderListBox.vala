using Gtk;
using Gdk;
namespace DHGWidgets {

public class DndOrderListBox : ListBox {

static CssProvider _provider = new CssProvider ();

const string DATA_ID = "DND_ORDER_LIST_BOX_ROW_DATA";

const string DEFAULT_CSS_RESOURCE = "/me/dhg/dndorderlistbox/DndOrderListBox.css";

public static string[] mgr_css_classes {get; set; default = {"place_up", "place_dn", "dragged", "dragging"};}

static TargetEntry[] entries = {
	TargetEntry () {
		target = DATA_ID, flags = TargetFlags.SAME_APP, info = 0
		}
	};

DndMgr _dnd_mgr;

public GLib.ListStore model {get; set;}

public static DndOrderListBoxBindings kbd_bindings {get; set;}

static construct {
	set_css_name ("dnd-order-list-box");
	_provider.load_from_resource(DEFAULT_CSS_RESOURCE);
	StyleContext.add_provider_for_screen (Screen.get_default(), _provider, STYLE_PROVIDER_PRIORITY_APPLICATION);
	kbd_bindings = new DndOrderListBoxBindings(BindingSet.by_class((ObjectClass) ( typeof ( DndOrderListBox )).class_ref()));
	}

public DndOrderListBox.with_list(GLib.ListStore list_model, ListBoxCreateWidgetFunc fn) {
	bind_list(list_model, fn);
	}

construct {
	_dnd_mgr = new DndMgr(this);
	}

void kbd_init () {
	if ( _dnd_mgr.keyboard_action != true ) {
		_dnd_mgr.keyboard_drag(get_selected_row());
		}
	}

void exchange_rows(int remove, int add, Object? source) {
	model.remove(remove);
	model.insert(add, source);
	}

[Signal (action = true)]
public virtual signal void kbd_vim () {
	if ( _dnd_mgr.keyboard_vim_mode ) {
		_dnd_mgr.dragged(false);
		} else {
		kbd_init();
		_dnd_mgr.keyboard_vim_mode = true;
		}
	}

[Signal (action = true)]
public virtual signal void kbd_move_indicator (bool dir) {
	kbd_init();
	_dnd_mgr.keyboard_move_mark(dir);
	}

[Signal (action = true)]
public virtual signal void kbd_move_cursor (MovementStep step, int count) {
	if ( _dnd_mgr.keyboard_action != true ) {
		move_cursor(step, count);
		}
	if ( _dnd_mgr.keyboard_vim_mode && step == MovementStep.DISPLAY_LINES ) {
		kbd_move_indicator(count != 1);
		}
	}

[Signal (action = true)]
public virtual signal void kbd_break () {
	if ( _dnd_mgr.keyboard_action == true ) {
		_dnd_mgr.dragged(false);
		}
	}

[Signal (action = true)]
public virtual signal void kbd_confirm () {
	if ( _dnd_mgr.keyboard_action == true ) {
		_dnd_mgr.dragged(false);
		exchange_rows(_dnd_mgr.source_position, _dnd_mgr.target_position, model.get_item(_dnd_mgr.source_position));
		}
	}

public void bind_list (GLib.ListStore list_model, ListBoxCreateWidgetFunc fn) {
	model = list_model;
	base.bind_model(model, ( obj ) => {return fn (obj);});
	_dnd_mgr.list_max = model.get_n_items();
	model.items_changed.connect(items_changed_cb);
	for ( int i = 0; i < _dnd_mgr.list_max; i++ ) {
		add_cb(get_row_at_index(i));
		}
	}

internal void add_cb (Widget row) {
	drag_source_set(row, BUTTON1_MASK, entries, DragAction.MOVE);
	drag_dest_set(row, DestDefaults.ALL, entries, DragAction.MOVE);
	row.drag_begin.connect(drag_begin_cb);
	row.button_press_event.connect(button_press_event_cb);
	row.drag_motion.connect(drag_motion_cb);
	row.drag_data_get.connect(drag_data_get_cb);
	row.drag_data_received.connect(drag_data_received_cb);
	row.drag_end.connect(drag_end_cb);
	}

void drag_end_cb (Widget row, DragContext c) {
	_dnd_mgr.dragged(false);
	}

void drag_data_received_cb (Widget row, DragContext c, int x, int y, SelectionData selection_data, uint info, uint time_) {
	_dnd_mgr.dragged(false);
	if ( _dnd_mgr.source == (ListBoxRow)row ) return;
	exchange_rows(_dnd_mgr.source_position, _dnd_mgr.target_position, ((Object[])selection_data.get_data ())[0]);
	}

void drag_data_get_cb (Widget row, DragContext c, SelectionData selection_data, uint info, uint time_) {
	uchar[] row_obj = new uchar[( sizeof ( Object ))];
	var data = model.get_item(_dnd_mgr.source_position);
	((Object[])row_obj )[0] = data;
	selection_data.@set(Atom.intern_static_string (DATA_ID), 32, row_obj);
	}

bool drag_motion_cb (Widget row, DragContext c, int x, int y, uint time_) {
	_dnd_mgr.update((ListBoxRow)row, y);
	return false;
	}

void drag_begin_cb (Widget row, DragContext context) {
	_dnd_mgr.pointer_drag((ListBoxRow)row);
	_dnd_mgr.drag_icon(true);
	var rect = Allocation();
	_dnd_mgr.source.get_allocation(out rect);
	Cairo.ImageSurface surface = new Cairo.ImageSurface (Cairo.Format.ARGB32, rect.width, rect.height);
	Cairo.Context ctx = new Cairo.Context (surface);
	_dnd_mgr.source.draw_to_cairo_context(ctx);
	surface.set_device_offset(-_dnd_mgr.start_x, -_dnd_mgr.start_y);
	drag_set_icon_surface(context, surface);
	_dnd_mgr.drag_icon(false);
	_dnd_mgr.dragged(true);
	}

bool button_press_event_cb (Widget row, EventButton event) {
	select_row((ListBoxRow)row);
	_dnd_mgr.start_x = (int) event.x;
	_dnd_mgr.start_y = (int) event.y;
	get_selected_row().grab_focus();
	return false;
	}

void items_changed_cb (uint position, uint removed, uint added) {
	_dnd_mgr.list_max = model.get_n_items();
	if ( added > 0 ) {
		for ( uint i = 0; i < added; i++ ) {
			add_cb(get_row_at_index((int)( i + position )));
			}
		if ( added == 1 && _dnd_mgr.target_position<_dnd_mgr.list_max ) {
			var new_row = get_row_at_index(_dnd_mgr.target_position);
			select_row(new_row);
			new_row.grab_focus();
			}
		}
	}

private new void add(){}
private new void remove() {}
private new void insert() {}
private new void set_selection_mode (SelectionMode m) {}
private new void bind_model (GLib.ListStore m, ListBoxCreateWidgetFunc f) {}

}
}
