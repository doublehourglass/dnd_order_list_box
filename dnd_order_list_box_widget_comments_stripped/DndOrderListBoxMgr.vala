using Gtk;
namespace DHGWidgets {

public class DndMgr : Object {

public Gtk.ListBox list_box {get; set;}

public struct CssClasses {
	string place_up;
	string place_dn;
	string dragged;
	string dragging;
	}

public CssClasses css_classes = CssClasses() {
	place_up = "place_up", place_dn = "place_dn", dragged = "dragged", dragging = "dragging"
	};

int distance = 0;
public ListBoxRow source;
ListBoxRow hovered;
StyleContext? marked_style;
StyleContext source_style;
StyleContext hovered_style;
bool? before_after;
public int start_x;
public int start_y;
public int hovered_position = 0;
public int source_position = 0;
public int target_position = 0;
public int half_height;
public uint list_max;
public bool keyboard_vim_mode;
public bool? keyboard_action;

[CCode (has_construct_function = false)]
public DndMgr(Gtk.ListBox list_box)
	{
	Object ( list_box: list_box );
	}

public void keyboard_drag (Gtk.ListBoxRow initial_row) {
	keyboard_action = true;
	keyboard_vim_mode = false;
	source = initial_row;
	source_style = source.get_style_context();
	source_position = source.get_index();
	distance = 0;
	target_position = source_position;
	dragged(true);
	}

public void keyboard_move_mark (bool dir) {
	distance += dir? ( target_position > 0? -1 : 0 ) : ( target_position < ( list_max - 1 )? 1 : 0 );
	target_position = source_position + distance;
	set_mark();
	}

public void pointer_drag (Gtk.ListBoxRow initial_row) {
	dragged(false);
	keyboard_action = false;
	source = initial_row;
	source_style = source.get_style_context();
	source_position = source.get_index();
	hovered = initial_row;
	update(initial_row, -1);
	}

public void update (Gtk.ListBoxRow? hovered_row, int y) {
	if ( hovered_row!=hovered ) {
		clear_mark();
		hovered = hovered_row;
		hovered_position = hovered.get_index();
		before_after = null;
		half_height = hovered.get_allocated_height() / 2;
		hovered_style = hovered.get_style_context();
		distance = hovered_position - source_position;
		}
	if ( y!=-1 ) {
		if ( ( y<half_height ) == before_after ) return;
		keyboard_action = false;
		if ( hovered_style == null ) return;
		if ( distance == 0 ) return;
		before_after = ( distance.abs() == 1 )? (( distance==1 )? false : true ) : y<half_height;
		if ( before_after == null ) {
			clear_mark();
			} else {
			if ( distance < 0 ) target_position = source_position + distance + ( before_after? 0 : 1 );
			if ( distance > 0 ) target_position = source_position + distance + ( before_after? -1 : 0 );
			set_mark();
			}
		}
	}

void clear_mark () {
	if ( marked_style == null ) return;
	marked_style.remove_class(css_classes.place_up);
	marked_style.remove_class(css_classes.place_dn);
	}

void set_mark () {
	clear_mark();
	if ( target_position == source_position ) return;
	marked_style = ( target_position == hovered_position )? hovered_style : list_box.get_row_at_index(target_position).get_style_context();
	marked_style.add_class(distance > 0? css_classes.place_dn : css_classes.place_up);
	}

public void dragged (bool mode) {
	if ( source_style != null ) {
		if ( mode ) {
			source_style.add_class(css_classes.dragged);
			}
		else {
			source_style.remove_class(css_classes.dragged);
			clear_mark();
			keyboard_vim_mode = false;
			}
		}
	keyboard_action = mode;
	}

public void drag_icon (bool mode) {
	if ( source_style != null ) {
		if ( mode ) {source_style.add_class(css_classes.dragging);}
		else{
			source_style.remove_class(css_classes.dragging);
			}
		}
	}

}
}
