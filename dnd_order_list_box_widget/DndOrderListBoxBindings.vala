using Gtk;
using Gdk;
namespace DHGWidgets {

/**
 * The ''DndOrderListBoxBindings'' is a utility class aimed at detaching keyboard
 * bindings logic from the widget body.
 *
 * It's intended to be extended in the future.
 */

public class DndOrderListBoxBindings : Object {

// A set of structs to make writing bindings easier.
public struct KeyBindingParam<G> {
	Type type;
	G param;
	public KeyBindingParam(G p, Type t) {
		this.param = p;
		this.type = t;
		}
	}

public struct KeyBinding {
	uint key;
	Gdk.ModifierType modifier;
	string signal_name;
	KeyBindingParam[] params;
	public KeyBinding(uint k, Gdk.ModifierType m, string s, KeyBindingParam[] p) {
		this.key = k;
		this.modifier = m;
		this.signal_name = s;
		this.params = p;
		}
	}

//why not write less?
public struct MoveKeyBinding {
	KeyBindingParam[] params;
	MoveKeyBinding(MovementStep step, int count) {
		params = {
			KeyBindingParam<MovementStep>(step, typeof( MovementStep )),
			KeyBindingParam<int>(count, typeof( int ))
			};
		}
	}

// Some shortcuting constants.
public const Gdk.ModifierType MSHFT = Gdk.ModifierType.SHIFT_MASK;
public const Gdk.ModifierType MCTRL = Gdk.ModifierType.CONTROL_MASK;
public const Gdk.ModifierType MNONE = ( Gdk.ModifierType ) 0;

// The below has added me another bunch of grey hair.
// Initially I wanted it to be a const, as it should be, but then generic cast
// of a KeyBindingParam caused strange errors.
// These errors contested my choice of bool? for dnd_mgr.keyboard_action.
// So, totally unrelated to this class (this was caused probably by a comparison
// in a signal body).
// Regardless however what I have changed to fix it, it threw lots of different
// errors all the time, so now it's static and maybe it even makes more sense.
//
// Also, I found no easy way to get a type of generic passed to KeyBindingParam
// from inside of its constructor.
// If someone knows a way - let me know.
/**
 * Standard keybindings. They override the parent's ones.
 */
public static KeyBinding[] KeyBindings = {
// Action keys
	KeyBinding(Gdk.Key.Up, MCTRL, "kbd_move_indicator", {KeyBindingParam<bool>(true, typeof( bool ))}),
	KeyBinding(Gdk.Key.Down, MCTRL, "kbd_move_indicator", {KeyBindingParam<bool>(false, typeof( bool ))}),
	KeyBinding(Gdk.Key.Return, MNONE, "kbd_confirm", {}),
	KeyBinding(Gdk.Key.Escape, MNONE, "kbd_break", {}),
// Home and End keys
	KeyBinding(Gdk.Key.Home, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, -1).params),
	KeyBinding(Gdk.Key.KP_Home, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, -1).params),
	KeyBinding(Gdk.Key.End, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, 1).params),
	KeyBinding(Gdk.Key.KP_End, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, 1).params),
// PageUp and PageDown keys
	KeyBinding(Gdk.Key.Page_Up, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.PAGES, -1).params),
	KeyBinding(Gdk.Key.KP_Page_Up, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.PAGES, -1).params),
	KeyBinding(Gdk.Key.Page_Down, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.PAGES, 1).params),
	KeyBinding(Gdk.Key.KP_Page_Down, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.PAGES, 1).params),
// Arrows
	KeyBinding(Gdk.Key.Up, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, -1).params),
	KeyBinding(Gdk.Key.KP_Up, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, -1).params),
	KeyBinding(Gdk.Key.Down, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, 1).params),
	KeyBinding(Gdk.Key.KP_Down, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, 1).params),
	};

/**
 * Vim style keybindings
 */
public static KeyBinding[] AlternativeKeyBindings = {
	KeyBinding(Gdk.Key.i, MNONE, "kbd_vim", {}),
	KeyBinding(Gdk.Key.k, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, -1).params),
	KeyBinding(Gdk.Key.j, MNONE, "kbd_move_cursor", MoveKeyBinding(MovementStep.DISPLAY_LINES, 1).params),
	KeyBinding(Gdk.Key.h, MSHFT, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, -1).params),
	KeyBinding(Gdk.Key.l, MSHFT, "kbd_move_cursor", MoveKeyBinding(MovementStep.BUFFER_ENDS, 1).params),
	KeyBinding(Gdk.Key.x, MNONE, "kbd_break", {}),
	};

/**
 * Constructs the class and initializes both binding sets.
 */
[CCode (has_construct_function = false)]
public static DndOrderListBoxBindings(BindingSet binding_set) {
	install_bindings(binding_set, KeyBindings);
	install_bindings(binding_set, AlternativeKeyBindings);
	}

/**
 * Enables run time change of bindings.
 */
public static void install_bindings(BindingSet bind_set, KeyBinding[] binding_list) {
	foreach ( KeyBinding k in binding_list ) {
		switch ( k.params.length ) {
		case 0:
			Gtk.BindingEntry.add_signal(bind_set, k.key, k.modifier, k.signal_name, 0);
			break;
		case 1:
			Gtk.BindingEntry.add_signal(bind_set, k.key, k.modifier, k.signal_name, 1, k.params[0].type, k.params[0].param);
			break;
		case 2:
			Gtk.BindingEntry.add_signal(bind_set, k.key, k.modifier, k.signal_name, 2, k.params[0].type, k.params[0].param, k.params[1].type, k.params[1].param);
			break;
			}
		}
	}


}
}
