# DndOrderListBox

The **DndOrderListBox** is a *Gtk.ListBox* descendant allowing for reordering its rows both with pointer or keyboard actions.
Contrary to some other implementations that use child widgets to enable drag'n'drop behavior the **DndOrderListBox** is widget agnostic and operates only on native rows.
The only caveat here is that child widgets must be able to receive drag events and thus must have own X11 windows.

## In action

Pointer operations:
![](presentation_gifs/pointer_ops.gif)

Keyboard operations:
![](presentation_gifs/keyboard_ops.gif)

## Limitations

The **DndOrderListBox** construction methods allow it to be populated only with a *GLib.ListModel* descendant (most likely *GLib.ListStore*).
However, as this component is a *work in progress* it may be extended to accept common *Gtk.ListBox* add, remove and insert methods at a later date.
There is neither way nor sense to enable multiselection abilities in this particular widget. So, they remain masked.

## Keyboard bindings

This widget provides users with extended keyboard bindings. Apart from standard *Gtk.ListBox* bindings (that are remapped) it comes with Vim-like ones by default.
Bindings are as follows:

### *moving selection*

**up** and **down** cursors  
**k** and **j** letters (*vim*)

### *initiating keyboard "drag"*

**\<Ctrl\> up** and **\<Ctrl\> down**  
**i** letter (*vim*)

### *moving "drop" marker*

**\<Ctrl\> up** and **\<Ctrl\> down** (so, to move marker \<Ctrl\> must stay depressed, it's like this to
be consistent with native *Gtk.ListBox* behavior of these keys)  
**k** and **j** letters (*vim*)

### *confirming*

**Enter** key

### *interrupting*

**Escape** key or pointer d'n'd action

## Repo contents

This repo contains both the bare widget and the demo app that shows it in action. There are two widget folders. I used this widget to learn Vala and therefore heavily documented every step - if your aim is to learn, pick the commented version of the widget. Otherwise I also left one with comments stripped, so it's easier to hack.

## Compilation of the demo app

To compile the demo app `cd` its folder and issue the following commands:
```
meson build
ninja -C build
```
To run the app either cd to `build/src` and run `./dnd_order_list_box_demo` or directly `build/src/./dnd_order_list_box_demo`.

## Generating documentation

A large part of the widget is documented using Valadoc syntax. To generate a HTML documentation from the repo's root folder issue a command:  
`valadoc -o valadoc dnd_order_list_box_widget/DndOrderList/*.vala --force --pkg gtk+-3.0`

Then, navigate to `valadoc/valadoc/index.html` and view it in a browser.

## Author

The **DndOrderListBox** was developed by **DHG** - mail me at doublehourglass|at|gmail.com.

## License

The **DndOrderListBox** is licensed under **GNU GPL 3.o.**

## Credits

Vector SVG images depicting fruits used in the example app designed by **Freepik**
