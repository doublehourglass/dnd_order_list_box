using Gdk;

namespace Dnd_order_list_box_demo {
[GtkTemplate (ui = "/me/dhg/dndorderlistboxdemo/about_dialog.ui")]
public class DndOrderListBoxDialog : Gtk.Dialog {
[GtkChild]
Gtk.Button about_dialog_close;
[GtkChild]
Gtk.Image about_dialog_icon;
construct {
	try {
		var p = new Pixbuf.from_resource_at_scale("/me/dhg/dndorderlistdemo/dnd_order_list_box_icon.svg", -1, 32, true);
		about_dialog_icon.set_from_pixbuf(p);
		} catch ( Error e ) {
		message("Error while loading an image. %s", e.message);
		}
	use_header_bar = 1;
	about_dialog_close.clicked.connect(( ) => {
				hide();
				});

	}

}
}
