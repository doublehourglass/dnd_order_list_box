using Gtk;
namespace DHGWidgets {

/**
 * The ''DndMgr'' is a utility class aimed at detaching a large part of logic
 * od drag'n'drop operations from its "parent" //DndOrderListBox// class.
 *
 * While full detachment isn't possible due to the way //Gtk.ListBox// operates,
 * this class takes care about counting positions of drag indicators, styling
 * and modes switching. Probably GTK4 will offer similar managers when issued.
 */

public class DndMgr : Object {

/**
 * The DndOrderListBox this manager is attached to.
 */
public Gtk.ListBox list_box {get; set;}

// A struct of CSS classes used by this manager
public struct CssClasses {
	string place_up;
	string place_dn;
	string dragged;
	string dragging;
	}

/**
 * CSS classes used to style dragged row and drag marker
 */
public CssClasses css_classes = CssClasses() {
	place_up = "place_up", place_dn = "place_dn", dragged = "dragged", dragging = "dragging"
	};

// The cached distance between the source widget and a currently selected one.
int distance = 0;
/**
 * The current source widget.
 */
public ListBoxRow source;
// A widget being currently hovered during the pointer drag operation.
ListBoxRow hovered;
// A cache for StyleContext of a widget being marked with an drag target
// indicator.
StyleContext? marked_style;
// A cache for StyleContext of the source widget.
StyleContext source_style;
// A cache for StyleContext of a hovered widget.
StyleContext hovered_style;
// The cached state of where the drag indicator should go.
bool? before_after;
/**
 * First of two cached values of initial pointer position in pointer drag.
 * Used by Cairo to set a proper offset of an icon (image representation of
 * a row).
 */
public int start_x;
/**
 * Second cached pointer coordinate.
 */
public int start_y;
/**
 * The cached position of a hovered during pointer drag row.
 */
public int hovered_position = 0;
/**
 * The cached position of the source row.
 */
public int source_position = 0;
/**
 * The cached target position.
 */
public int target_position = 0;
/**
 * The cached half height value of a currently hovered row - used to save calls
 * to measure a widget.
 */
public int half_height;
/**
 * The cached value of length of the ListModel attached to the DndOrderListBox.
 */
public uint list_max;
/**
 * The cached vim mode state.
 */
public bool keyboard_vim_mode;
/**
 * The cached keyboard action state.
 */
public bool? keyboard_action;

/**
 * Creates a new manager, accepts only one parameter - a DndOrderListBox to manage.
 */
[CCode (has_construct_function = false)]
public DndMgr(Gtk.ListBox list_box)
	{
	Object ( list_box: list_box );
	}

/**
 * Called by a listbox signal handler when appropriate keys are activated.
 * Populates the manager with received data.
 */
public void keyboard_drag (Gtk.ListBoxRow initial_row) {
	keyboard_action = true;
	keyboard_vim_mode = false;
	source = initial_row;
	source_style = source.get_style_context();
	source_position = source.get_index();
	distance = 0;
	target_position = source_position;
	dragged(true);
	}

/**
 * Moves a "drag" mark by keyboard in the indicated direction.
 */
public void keyboard_move_mark (bool dir) {
	distance += dir? ( target_position > 0? -1 : 0 ) : ( target_position < ( list_max - 1 )? 1 : 0 );
	target_position = source_position + distance;
	set_mark();
	}

/**
 * Initiates data needed to perform drag by pointer.
 */
public void pointer_drag (Gtk.ListBoxRow initial_row) {
	dragged(false);
	keyboard_action = false;
	source = initial_row;
	source_style = source.get_style_context();
	source_position = source.get_index();
	hovered = initial_row;
	update(initial_row, -1);
	}

/**
 * Updates drag by pointer data.
 */
public void update (Gtk.ListBoxRow? hovered_row, int y) {
	if ( hovered_row!=hovered ) {
		clear_mark();
		hovered = hovered_row;
		hovered_position = hovered.get_index();
		before_after = null;
		half_height = hovered.get_allocated_height() / 2;
		hovered_style = hovered.get_style_context();
		distance = hovered_position - source_position;
		}
	if ( y!=-1 ) {
		if ( ( y<half_height ) == before_after ) return;
		keyboard_action = false;
		if ( hovered_style == null ) return;
		if ( distance == 0 ) return;
		before_after = ( distance.abs() == 1 )? (( distance==1 )? false : true ) : y<half_height;
		if ( before_after == null ) {
			clear_mark();
			} else {
			if ( distance < 0 ) target_position = source_position + distance + ( before_after? 0 : 1 );
			if ( distance > 0 ) target_position = source_position + distance + ( before_after? -1 : 0 );
			set_mark();
			}
		}
	}

// Utility method - clears a mark.
void clear_mark () {
	if ( marked_style == null ) return;
	marked_style.remove_class(css_classes.place_up);
	marked_style.remove_class(css_classes.place_dn);
	}

// Utility method - sets a mark.
void set_mark () {
	clear_mark();
	if ( target_position == source_position ) return;
	marked_style = ( target_position == hovered_position )? hovered_style : list_box.get_row_at_index(target_position).get_style_context();
	marked_style.add_class(distance > 0? css_classes.place_dn : css_classes.place_up);
	}

/**
 * Make the source row displayed in "being dragged" style.
 */
public void dragged (bool mode) {
	if ( source_style != null ) {
		if ( mode ) {
			source_style.add_class(css_classes.dragged);
			}
		else {
			source_style.remove_class(css_classes.dragged);
			clear_mark();
			keyboard_vim_mode = false;
			}
		}
	keyboard_action = mode;
	}
/**
 * Make the source row temporarily styled with appropriate background - used
 * by Cairo.
 */
public void drag_icon (bool mode) {
	if ( source_style != null ) {
		if ( mode ) {source_style.add_class(css_classes.dragging);}
		else{
			source_style.remove_class(css_classes.dragging);
			}
		}
	}


}
}
