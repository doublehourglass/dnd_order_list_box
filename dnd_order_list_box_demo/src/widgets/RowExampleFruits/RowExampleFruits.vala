using Gdk;
namespace Samples {

[GtkTemplate (ui = "/me/dhg/rowexamplefruits/RowExampleFruits.ui")]
public class RowExampleFruits : Gtk.EventBox {
[GtkChild]
Gtk.Image fruits_img;
[GtkChild]
Gtk.Label fruits_label;
public RowExampleFruits(string image_res, string text) {
	try {
		var p = new Pixbuf.from_resource_at_scale(image_res, -1, 32, true);
		fruits_img.set_from_pixbuf(p);
	} catch (Error e) {
		message("Error while loading an image. %s", e.message);
	}
	fruits_label.set_label(text);
}

}
}
