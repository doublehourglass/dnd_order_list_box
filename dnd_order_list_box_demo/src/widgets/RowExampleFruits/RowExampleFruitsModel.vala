namespace Samples {


public class RowExampleFruitsItem : Object {
public string image_res {get; set;}
public string text {get; set;}

public RowExampleFruitsItem(string image_res, string text) {
	Object (
		image_res: image_res,
		text: text
		);
	}
}
}
