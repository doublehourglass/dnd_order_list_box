/* window.vala
 *
 * Copyright 2019 doublehourglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using DHGWidgets;
using Samples;

namespace Dnd_order_list_box_demo {
[GtkTemplate (ui = "/me/dhg/dndorderlistboxdemo/window.ui")]
public class Window : Gtk.ApplicationWindow {
[GtkChild]
Gtk.Button about_button;

public struct RowData {
	string image_res;
	string text;
	public RowData(string i, string t) {
		this.image_res = i;
		this.text = t;
		}
	}

public RowData[] rows = {
	RowData("apple.svg", "Apple"),
	RowData("banana.svg", "Banana"),
	RowData("cherry.svg", "Cherry"),
	RowData("blueberry.svg", "Blueberry"),
	RowData("fig.svg", "Fig"),
	RowData("watermelon.svg", "Watermelon"),
	RowData("raspberry.svg", "Raspberry"),
	RowData("strawberry.svg", "Strawberry"),
	RowData("apricot.svg", "Apricot"),
	RowData("mango.svg", "Mango"),
	RowData("orange.svg", "Orange"),
	RowData("lemon.svg", "Lemon"),
	RowData("grapes.svg", "Grapes"),
	};

public GLib.ListStore list_store = new GLib.ListStore(typeof( RowExampleFruitsItem ));
public SimpleAction dialog_action;
public DndOrderListBoxDialog about_dialog;

public Window (Gtk.Application app) {
	Object (
		application: app,
		window_position: Gtk.WindowPosition.CENTER
		);
	var dnd_list_box = new DndOrderListBox();
	for ( int i = 0; i < rows.length; i++ ) {
		list_store.append(new RowExampleFruitsItem("/me/dhg/rowexamplefruits/" + rows[i].image_res, rows[i].text));
		}
	dnd_list_box.bind_list(list_store, ( obj ) => {
				return new RowExampleFruits(((RowExampleFruitsItem)obj ).image_res, ((RowExampleFruitsItem)obj ).text);
				});
	add(dnd_list_box);
	about_button.clicked.connect (( ) => {
				if ( about_dialog==null ) {
					about_dialog = new DndOrderListBoxDialog();
					about_dialog.set_transient_for (this);
					about_dialog.run();
					} else {
					about_dialog.show();
					}
				});

	}
}
}
