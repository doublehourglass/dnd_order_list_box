/* main.vala
 *
 * Copyright 2019 doublehourglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class DndOrderListBoxDemo : Gtk.Application {

public DndOrderListBoxDemo () {
	Object (
		application_id: "me.dhg.dndorderlistboxdemo",
		flags : ApplicationFlags.FLAGS_NONE
		);
	_instance = this;
	}
public static DndOrderListBoxDemo _instance = null;
public static DndOrderListBoxDemo instance {
	get {
		if ( _instance == null ) {
			_instance = new DndOrderListBoxDemo ();
			}
		return _instance;
		}
	}
protected override void activate () {
	if ( get_windows ().length () > 0 ) {
		get_windows ().data.present ();
		return;
		}

	var main_window = new Dnd_order_list_box_demo.Window (this);
	main_window.show_all ();


	}
}

int main (string[] args) {
	Gtk.init (ref args);
	var app = new DndOrderListBoxDemo ();
	return app.run (args);
	}
